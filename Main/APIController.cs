﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class APIRequestModel
{
    public string Command { get; set; }
    public List<string> Parameters { get; set; }

    public string FirstParamter
    {
        get { return Parameters.FirstOrDefault() ?? ""; }
    }
}

public class APIController : MonoBehaviour
{

    private UniWebServer.WebServer server;

    // Use this for initialization
    void Start ()
    {
        server = new UniWebServer.WebServer(Constants.WebServer.Port, 2, true);
        server.HandleRequest += Server_HandleRequest;
        server.Start();
    }

    private void Server_HandleRequest(UniWebServer.Request request, UniWebServer.Response response)
    {
        response.statusCode = 200;
        APIRequestModel model = null;
        if (!string.IsNullOrEmpty(request.body))
        {
            model = JsonConvert.DeserializeObject<APIRequestModel>(request.body); 
        }
        switch (model.Command)
        {
            case "speak":
                switch (model.FirstParamter)
                {
                    case "start":
                        //on fait parler l'avatar
                        break;
                    case "stop":
                        //on fait parler l'avatar
                        break;
                    default:
                        break;
                }
                break;
            default:
                response.statusCode = 404;
                break;
        }
    }

    // Update is called once per frame
    void Update ()
    {
		
	}
}
