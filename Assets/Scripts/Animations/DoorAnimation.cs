﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAnimation : MonoBehaviour {

    public GameObject leftDoor;
    public GameObject rightDoor;
    public float duration = 2f;
    public float distance = 1f;

    void Start()
    {

    }

    public void StartAnimationOpenDoors()
    {

        var step1 = distance * 0.5f;
        var step2 = distance * 0.75f;

        var pathLeft = new Vector3[] { leftDoor.transform.position, leftDoor.transform.position + (Vector3.left * step1) , leftDoor.transform.position + Vector3.left * step2, leftDoor.transform.position + Vector3.left * distance};
        var pathRight = new Vector3[] { rightDoor.transform.position, rightDoor.transform.position + (Vector3.right * step1), rightDoor.transform.position + Vector3.right * step2, rightDoor.transform.position + Vector3.right *distance };

        LeanTween.move(leftDoor, pathLeft, this.duration).setOrientToPath2d(true);
        LeanTween.move(rightDoor, pathRight, this.duration).setOrientToPath2d(true);
    }

    private void OnTriggerEnter()
    { 
        StartAnimationOpenDoors();
    }
}
