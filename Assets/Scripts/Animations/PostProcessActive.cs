﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessActive : MonoBehaviour {



    // Use this for initialization
    void Start () {

            var vignette = ScriptableObject.CreateInstance<Vignette>();
            vignette.enabled.Override(true);
            vignette.intensity.Override(1f);

           // var volume = PostProcessManager.instance.QuickVolume(gameObject.layer, 100f, vignette);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
