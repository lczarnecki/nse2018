﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimation : MonoBehaviour
{
    private GameObject cameraObject;
    public float duration = 10;
    public float distance = 5;

    void Start()
    {
        cameraObject = GameObject.Find("FrontCamera");
    }

    public void StartAnimationForward()
    {
        var step1 = distance * 0.5f;
        var step2 = distance * 0.75f;


        var path = new Vector3[] { cameraObject.transform.position, cameraObject.transform.position + Vector3.forward * step1, cameraObject.transform.position + Vector3.forward * step2, cameraObject.transform.position + Vector3.forward * distance };
        LeanTween.move(cameraObject, path, this.duration).setOrientToPath2d(true);
    }

}
