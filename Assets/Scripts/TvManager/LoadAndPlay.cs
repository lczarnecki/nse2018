﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class LoadAndPlay : MonoBehaviour {

	public List<GameObject> Tvs;

	public void PlayVideoOnTv(string videoName, string labelVideo, int tvId)
	{
		StartCoroutine(CoroutineNoiseAndVideo(videoName, labelVideo, tvId));
	}

    private void PlayVideo(string videoName, int tvId)
	{
		var clip = Resources.Load<VideoClip>("Videos/Reponses/" + videoName);
        var vpTv = Tvs[tvId].GetComponentInChildren<VideoPlayer>();
		vpTv.clip = clip;
		vpTv.Play();      
	}

	IEnumerator CoroutineNoiseAndVideo(string videoName, string labelVideo, int tvId)
	{
        var light = Tvs[tvId].GetComponentInChildren<Light>();
        var txt = Tvs[tvId].GetComponentInChildren<TextMesh>();
        txt.text = "";
        light.intensity = 0f;

        PlayVideo("noise", tvId);

        light.intensity = 4.0f;
        yield return new WaitForSeconds(1.5f);
        txt.text = labelVideo;

        PlayVideo(videoName, tvId);

	}
    
}
