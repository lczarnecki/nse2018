﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;



public class LoadFinalVideo : MonoBehaviour {

    private VideoPlayer vp;

	// Use this for initialization
	void Start () {
		
	}


    private void PlayVideo(string videoName)
    {
        var clip = Resources.Load<VideoClip>("Videos/Reponses/" + videoName);
        vp = GetComponentInChildren<VideoPlayer>();
        Debug.Log(clip.name);
        vp.clip = clip;
        Debug.Log(" " + vp.clip.name);

        vp.Play();
    }


    // Update is called once per frame
    void Update () {
	 if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("play");
            PlayVideo("equipe");
        }
	}
}
