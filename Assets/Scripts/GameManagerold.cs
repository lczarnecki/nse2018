﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerOld: MonoBehaviour
{

    public GameObject goPath;
    public GameObject goTrap;

    public int lengthLevel = 200;

    private int[] left;
    private int[] middle;
    private int[] right;

    struct Cubes
    {
        public GameObject cubel;
        public GameObject cubem;
        public GameObject cuber;
    }

    private Cubes[] goCubes;

    // Use this for initialization
    void Start()
    {
        // Init
        left = new int[lengthLevel];
        middle = new int[lengthLevel];
        right = new int[lengthLevel];

        goCubes = new Cubes[lengthLevel];

        GenerateLevel();
        GenerateCubes();
    }

    private void TrapOrPath(int i)
    {
        int rand = Random.Range(0, 2);
        if (rand == 0)
        {
            left[i] = 2;
            middle[i] = 0;
            right[i] = 1;
        }
        else
        {
            left[i] = 1;
            middle[i] = 0;
            right[i] = 2;
        }
    }

    private void GenerateLev(int min, int max, float percent)
    {
        for (int i = min; i < max; i++)
        {
            left[i] = 0;
            middle[i] = 1;
            right[i] = 0;
        }

        int nb = (int)((max - min) * percent);
        for( int i = 0; i < nb; i++)
        {
            int rand = Random.Range(min, max);
            TrapOrPath(rand);
        }
    }

    private void GenerateLevel()
    {
        GenerateLev(0, 5, 0.0f);
        GenerateLev(5, 20, 0.05f);
        GenerateLev(20, 50, 0.1f);
        GenerateLev(50, 100, 0.15f);
        GenerateLev(100, lengthLevel, 0.2f);
    }

    private void GenerateCubes()
    {

        GameObject clone;

        for (int i = 0; i < lengthLevel; i++)
        {
            if(middle[i] == 1)
            {
                clone = Instantiate(goPath, new Vector3(i, 0, 0), Quaternion.identity);
                clone.transform.parent = this.transform;
                goCubes[i].cubem = clone;
            } else
            {
                if (left[i] == 1)
                {
                    clone = Instantiate(goPath, new Vector3(i, 0, 1.5f), Quaternion.identity);
                    clone.transform.parent = this.transform;
                    clone.GetComponent<CubeId>().id = i;
                    goCubes[i].cubel = clone;
                    clone = Instantiate(goTrap, new Vector3(i, 0, -1.5f), Quaternion.identity);
                    clone.transform.parent = this.transform;
                    clone.GetComponent<CubeId>().id = i;
                    goCubes[i].cuber = clone;
                } else
                {
                    clone = Instantiate(goPath, new Vector3(i, 0, -1.5f), Quaternion.identity);
                    clone.transform.parent = this.transform;
                    clone.GetComponent<CubeId>().id = i;
                    goCubes[i].cubel = clone;
                    clone = Instantiate(goTrap, new Vector3(i, 0, 1.5f), Quaternion.identity);
                    clone.transform.parent = this.transform;
                    clone.GetComponent<CubeId>().id = i;
                    goCubes[i].cuber = clone;
                }
            }
        }
    }

    public void CubeClicked(int i)
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position -= new Vector3(0.15f, 0, 0);
        if (Input.GetMouseButtonDown(0))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                if(hit.transform.GetComponent<CubeId>().id != 0)
                {
                    int i = hit.transform.GetComponent<CubeId>().id;
                    Debug.Log(i);
                    if(goCubes[i].cubel == hit.transform.gameObject)
                    {
                        goCubes[i].cubel.transform.position = new Vector3(goCubes[i].cubel.transform.position.x, 0, 0);
                        Object.Destroy(goCubes[i].cuber);
                    } else
                    {
                        goCubes[i].cuber.transform.position = new Vector3(goCubes[i].cuber.transform.position.x, 0, 0);
                        Object.Destroy(goCubes[i].cubel);
                    }
                }
            }
        }
    }
}
