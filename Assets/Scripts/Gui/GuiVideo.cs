﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiVideo : MonoBehaviour {


    public enum ResponseVideoEnum { effortDistance, equipe, interieur, mer, montagne, performance, produitInnovant, relax, sensationForte, stratege, surf, ville };

    public class ResponseVideo
    {
        public string Label { get; set; }
        public string VideoName { get; set; }
    }


	private LoadAndPlay videoManager;
    private Dictionary<ResponseVideoEnum, ResponseVideo> Videos;


	private void Start()
	{
		videoManager = GetComponent<LoadAndPlay>();
        Videos = new Dictionary<ResponseVideoEnum, ResponseVideo>();
        loadVideoList();
	}

    private void loadVideoList()
    {
        ResponseVideo item = new ResponseVideo();
        item.Label = "Endurance";
        item.VideoName = "effort-distance";
        Videos.Add(ResponseVideoEnum.effortDistance, item);

        item = new ResponseVideo();
        item.Label = "Equipe";
        item.VideoName = "equipe";
        Videos.Add(ResponseVideoEnum.equipe, item);

        item = new ResponseVideo();
        item.Label = "Intérieur";
        item.VideoName = "interieur";
        Videos.Add(ResponseVideoEnum.interieur, item);

        item = new ResponseVideo();
        item.Label = "Mer";
        item.VideoName = "mer";
        Videos.Add(ResponseVideoEnum.mer, item);

        item = new ResponseVideo();
        item.Label = "Performance";
        item.VideoName = "performance";
        Videos.Add(ResponseVideoEnum.performance , item);

        item = new ResponseVideo();
        item.Label = "Produit innovant";
        item.VideoName = "produit-innovant";
        Videos.Add(ResponseVideoEnum.produitInnovant, item);

        item = new ResponseVideo();
        item.Label = "Prendre son temps" + "\n" + "instant présent"  ;
        item.VideoName = "relax";
        Videos.Add(ResponseVideoEnum.relax, item);

        item = new ResponseVideo();
        item.Label = "Montagne";
        item.VideoName = "montagne";
        Videos.Add(ResponseVideoEnum.montagne, item);

        item = new ResponseVideo();
        item.Label = "Sensation forte";
        item.VideoName = "sensation-forte";
        Videos.Add(ResponseVideoEnum.sensationForte, item);

        item = new ResponseVideo();
        item.Label = "Stratege";
        item.VideoName = "stratege";
        Videos.Add(ResponseVideoEnum.stratege, item);

    }


	private void OnGUI()
    {
        GUILayout. BeginVertical(GUILayout.Width(Screen.width));
		GUILayout.Space(75);

		GUILayout.BeginHorizontal();
        if (GUILayout.Button("TV1L"))
        {
            videoManager.PlayVideoOnTv(Videos[ResponseVideoEnum.relax].VideoName, Videos[ResponseVideoEnum.mer].Label, 0);
        }

		if (GUILayout.Button("TV2L"))
        {
            videoManager.PlayVideoOnTv(Videos[ResponseVideoEnum.performance].VideoName, Videos[ResponseVideoEnum.performance].Label, 1);
        }

		if (GUILayout.Button("TV3L"))
        {
            videoManager.PlayVideoOnTv(Videos[ResponseVideoEnum.effortDistance].VideoName, Videos[ResponseVideoEnum.effortDistance].Label, 2);
        }

        if (GUILayout.Button("TV1R"))
        {
            videoManager.PlayVideoOnTv(Videos[ResponseVideoEnum.montagne].VideoName, Videos[ResponseVideoEnum.montagne].Label, 3);
        }

        if (GUILayout.Button("TV2R"))
        {
            videoManager.PlayVideoOnTv(Videos[ResponseVideoEnum.sensationForte].VideoName, Videos[ResponseVideoEnum.sensationForte].Label, 4);
        }

        if (GUILayout.Button("TV3R"))
        {
            videoManager.PlayVideoOnTv(Videos[ResponseVideoEnum.stratege].VideoName, Videos[ResponseVideoEnum.stratege].Label, 5);
        }


        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
        
    }
}
