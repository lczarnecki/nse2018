﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventSpeekScript : MonoBehaviour {

	public UnityEvent EventStartSpeak;
	public UnityEvent EventStopSpeak;
	public UnityEvent EventSmile;
	public UnityEvent EventUnhappy;
	public UnityEvent EventBlinkRightEye;
	public UnityEvent EventBlinkLeftEye;

	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.U))
		{
			EventStartSpeak.Invoke();
		}

		if (Input.GetKey(KeyCode.J))
        {
			EventStopSpeak.Invoke();
        }

		if (Input.GetKey(KeyCode.I))
        {
			EventSmile.Invoke();
        }

		if (Input.GetKey(KeyCode.O))
        {
			EventUnhappy.Invoke();
        }
        
		if (Input.GetKey(KeyCode.Keypad7))
        {
			EventBlinkRightEye.Invoke();
        }

        if (Input.GetKey(KeyCode.Keypad8))
        {
			EventBlinkLeftEye.Invoke();
        }
	}



}
