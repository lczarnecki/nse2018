using System;
using UnityEngine;


[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
[RequireComponent(typeof(CharacterAutoControl))]
public class AICharacterControl : MonoBehaviour
{
	public UnityEngine.AI.NavMeshAgent agent { get; private set; }             
	public CharacterAutoControl character { get; private set; } 

	private Transform target;                              

	private void Start()
	{
		agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
		character = GetComponent<CharacterAutoControl>();

		agent.updateRotation = false;
		agent.updatePosition = true;
	}


	private void Update()
	{
		if (target != null)
		{
			agent.SetDestination(target.position);
		}

		if (agent.remainingDistance > agent.stoppingDistance)
		{
			character.Move((agent.desiredVelocity), target);
		}
		else
		{
			character.Move(Vector3.zero, target);
		}
	}

	public void SetTarget(Transform target)
	{
		this.target = target;
	}
}

