﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum MoveCharacterEnum { center, left, right, onSide }

[RequireComponent(typeof(AICharacterControl))]
[RequireComponent(typeof(SpeechAnimScript))]
[RequireComponent(typeof(EyesAnimScript))]
public class CharacterManager : MonoBehaviour {

    private AICharacterControl aICharacter;
    private SpeechAnimScript speechAnim;
    private EyesAnimScript eyesAnim;

    public GameObject TargetRight;
    public GameObject TargetLeft;
    public GameObject TargetCenter;
    public GameObject TargetOnSide;

    void Start()
    {
        aICharacter = GetComponent<AICharacterControl>();
        aICharacter.SetTarget(TargetCenter.transform);

        speechAnim = GetComponent<SpeechAnimScript>();
        eyesAnim = GetComponent<EyesAnimScript>();
    }

    public void SelectTarget(MoveCharacterEnum moveCharacter)
    {

        
        switch (moveCharacter)
        {
            case MoveCharacterEnum.left: //Left target
                aICharacter.SetTarget(TargetLeft.transform);
                break;
            case MoveCharacterEnum.right: //Right target
                aICharacter.SetTarget(TargetRight.transform);
                break;
            case MoveCharacterEnum.onSide: // On side target
                aICharacter.SetTarget(TargetOnSide.transform);
                break;
            default: //Center target
                aICharacter.SetTarget(TargetCenter.transform);
                break;
        }
    }

    public void SelectExpression(EyesAnimScript.ExpressionFacialEnum expression = EyesAnimScript.ExpressionFacialEnum.idle , float durationReturnIdle = 1.5f)
    {
        switch (expression)
        {
            case EyesAnimScript.ExpressionFacialEnum.smile: //Smile
                eyesAnim.expression = EyesAnimScript.ExpressionFacialEnum.smile;
                speechAnim.expression = (speechAnim.expression == SpeechAnimScript.MounthExpressionEnum.speech) ? SpeechAnimScript.MounthExpressionEnum.speech : SpeechAnimScript.MounthExpressionEnum.smile;
                StartCoroutine(CoroutineWaitReturnIdle(durationReturnIdle));
                break;
            case EyesAnimScript.ExpressionFacialEnum.unhappy: //Unhappy
                eyesAnim.expression = EyesAnimScript.ExpressionFacialEnum.unhappy;
                speechAnim.expression = (speechAnim.expression == SpeechAnimScript.MounthExpressionEnum.speech) ? SpeechAnimScript.MounthExpressionEnum.speech : SpeechAnimScript.MounthExpressionEnum.unhappy;
                StartCoroutine(CoroutineWaitReturnIdle(durationReturnIdle));
                break;
            case EyesAnimScript.ExpressionFacialEnum.none: //none
                eyesAnim.expression = EyesAnimScript.ExpressionFacialEnum.none;
                speechAnim.expression = (speechAnim.expression == SpeechAnimScript.MounthExpressionEnum.speech) ? SpeechAnimScript.MounthExpressionEnum.speech : SpeechAnimScript.MounthExpressionEnum.none;
                StartCoroutine(CoroutineWaitReturnIdle(durationReturnIdle));
                break;
            default:
                eyesAnim.expression = EyesAnimScript.ExpressionFacialEnum.idle;
                speechAnim.expression = (speechAnim.expression == SpeechAnimScript.MounthExpressionEnum.speech) ? SpeechAnimScript.MounthExpressionEnum.speech : SpeechAnimScript.MounthExpressionEnum.none;
                break;
        }
    }

    IEnumerator CoroutineWaitReturnIdle(float duration)
    {
        yield return new WaitForSeconds(duration);
        eyesAnim.expression = EyesAnimScript.ExpressionFacialEnum.idle;
    }


    public void StartHelloAnimation()
    {
        aICharacter.character.WaveTrigger();
    }

    public void StartPickupAnimation()
    {
        aICharacter.character.PickupTrigger();
    }

    public void StartSpeak()
    {
        speechAnim.expression = SpeechAnimScript.MounthExpressionEnum.speech;
    }

    public void StopSpeak()
    {
        speechAnim.expression = SpeechAnimScript.MounthExpressionEnum.none;
    }

}
