﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeechAnimScript : MonoBehaviour {
    
	public enum MounthExpressionEnum {none, speech, smile, unhappy};
	public MounthExpressionEnum expression = MounthExpressionEnum.none;

	public int uvTileY = 4; // texture sheet columns 
    public int uvTileX = 3; // texture sheet rows
	public float RateSpeedExpression = 0.2f;
    

    private Vector2 size;  
    private Vector2 offset;

	private float nextExpression = 0f;
	private Renderer rendererObject;
    
	// Use this for initialization
	void Start () {
		this.rendererObject = GetComponentInChildren<Renderer>(); 
		this.size = new Vector2(1.0f / uvTileY, 1.0f / uvTileX);
		DrawImageByIndex(0);
	}

	private void StartSpeechText()
	{
		this.expression = MounthExpressionEnum.speech;
	}   

	private void StopSpeechText()
    {
		this.expression = MounthExpressionEnum.none;
    }   

    private void SmileAnim()
	{
		//this.expression = ExpressionEnum.smile;
		DrawImageByIndex(8);
	}

    private void unHappyAnim()
	{
		//this.expression = ExpressionEnum.unhappy;
        DrawImageByIndex(11);
	}

	private void SpeechText()
	{
		if (nextExpression < Time.time)
		{
			DrawImageByIndex(Random.Range(0, (this.uvTileX * this.uvTileY) - 1)); // select random number in terms of colw and rows texture
			this.nextExpression = Time.time + this.RateSpeedExpression;
		}
	}

	private void EndSpeechText()
    {
        DrawImageByIndex(6);
    }

    private void DrawImageByIndex(int index)
	{
		var uIndex = index % this.uvTileY;
		var vIndex = index / this.uvTileY;
       
		this.offset = new Vector2(uIndex * this.size.x, 1.0f - this.size.y - vIndex * this.size.y);
		this.rendererObject.materials[3].SetTextureOffset("_MainTex", this.offset);
		this.rendererObject.materials[3].SetTextureScale("_MainTex", this.size);

	}

  

	void Update()
	{
		switch (expression)
		{
			case MounthExpressionEnum.speech:
				SpeechText();
				break;
			case MounthExpressionEnum.smile:
				SmileAnim();
				break;
			case MounthExpressionEnum.unhappy:
				unHappyAnim();
				break;
			default:
				EndSpeechText();
				break;
		}

	}

 
}
