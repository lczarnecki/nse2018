﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private FadeInFadeOutManager fadeManager;
    private CameraAnimation cameraAnimation;
    private CharacterManager characterManager;
    private LoadAndPlay loadAndPlayManager;

    // Use this for initialization
    void Start()
    {
        fadeManager = GameObject.Find("Managers/FadeManager").GetComponentInChildren<FadeInFadeOutManager>();
        cameraAnimation = GameObject.Find("Managers/CameraManager").GetComponentInChildren<CameraAnimation>();
        characterManager = GameObject.Find("Maria").GetComponentInChildren<CharacterManager>();
        loadAndPlayManager = GameObject.Find("WallTV").GetComponentInChildren<LoadAndPlay>();
    }


    public void FadeCamera(bool fade, FadeCameraEnum fadeCameraEnum)
    {
        fadeManager.Fade(fade, fadeCameraEnum);
    }

    public void CameraAnimation()
    {
        cameraAnimation.StartAnimationForward();
    }

    public void StartSpeak()
    {
        characterManager.StartSpeak();
    }

    public void StopSpeak()
    {
        characterManager.StopSpeak();
    }

    public void SelectExpression(EyesAnimScript.ExpressionFacialEnum expression = EyesAnimScript.ExpressionFacialEnum.idle, float durationReturnIdle = 1.5f)
    {
        characterManager.SelectExpression(expression, durationReturnIdle);
    }

    public void SelectTarget(MoveCharacterEnum moveCharacter)
    {
        characterManager.SelectTarget(moveCharacter);
    }
}
